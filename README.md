# docker



## Repository details

This repository contains a defined Dockerfile for several types of operating systems.

Adapt as need, dont use the defaults.

## Game Dockerfile templates

Urban Terror and Minecraft models require manual installation of the game and server config files.

The Dockerfile provides only ssh server and default port opened of the game

## Jenkins

This Dockerfile is based on the LTS version available at 20/02/2023

The LTS version might change over time and the requirements