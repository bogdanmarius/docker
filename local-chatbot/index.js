const express = require('express');
const path = require('path');

const app = express();
const port = 5000;

// Define the questions and answers
const qaPairs = {
    "hello": "Hi there! How can I help you today?",
    "how are you": "I'm a chatbot, so I'm always good! How about you?",
    "bye": "Goodbye! Have a nice day!",
    // Add more questions and answers here
};

// Function to get the answer based on the question
const getAnswer = (question) => {
    const normalizedQuestion = question.toLowerCase();
    return qaPairs[normalizedQuestion] || "I'm sorry, I don't understand that question.";
};

// Serve static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());

// Handle chat requests
app.post('/chat', (req, res) => {
    const userQuestion = req.body.question;
    const answer = getAnswer(userQuestion);
    res.json({ question: userQuestion, answer: answer });
});

app.listen(port, () => {
    console.log(`Chatbot server listening at http://localhost:${port}`);
});
